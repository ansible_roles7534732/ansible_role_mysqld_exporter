Mysqld_exporter role
=========

Installs mysqld_exporter(https://github.com/prometheus/mysqld_exporter)

Requirements
------------

—

Role Variables
--------------

See defaults/main.yml

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install mysqld_exporter
  hosts: db_vms
  become: true
  gather_facts: true
  roles:
    - mysqld_exporter
      tags:
        - role_mysqld_exporter
```

License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
